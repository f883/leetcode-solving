/**
 * @param {number[][]} matrix
 * @param {number} target
 * @return {boolean}
 */
var searchMatrix = function(matrix, target) {
  const xLen = matrix.length;
  const yLen = matrix[0].length;

  for (let i = 0; i < xLen; i++){
      if (matrix[i][yLen - 1] >= target){
          for (let j = 0; j < yLen; j++){
              if (matrix[i][j] === target) return true;
          }
      }
  }

  return false;
};