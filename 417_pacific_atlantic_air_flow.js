/**
 * @param {number[][]} heights
 * @return {number[][]}
 */
var pacificAtlantic = function (heights) {
  function dfsPo(map, i, j, lastHeight) {
    if (i >= heights.length || j >= heights[0].length || i < 0 || j < 0) return;
    
    const curVal = heights[i][j];
    const ind = `${i},${j}`;

    if (heights[i][j] < lastHeight) return;

    if (map[ind]) return;
    
    map[ind] = true;
    
    dfsPo(map, i + 1, j, curVal);
    dfsPo(map, i, j + 1, curVal);
    dfsPo(map, i - 1, j, curVal);
    dfsPo(map, i, j - 1, curVal);
  }

  const poArr = {};
  const aoArr = {};

  for (let i = 0; i < heights.length; i++) {
    dfsPo(poArr, i, 0, 0);
    dfsPo(aoArr, i, heights[0].length - 1, 0);
  }
  for (let i = 0; i < heights[0].length; i++) {
    dfsPo(poArr, 0, i, 0);
    dfsPo(aoArr, heights.length - 1, i, 0);
  }

  const res = [];

    Object.keys(aoArr).forEach(el => {
      if (poArr[el]) res.push(el.split(','))
  })
  
  return res;
};
