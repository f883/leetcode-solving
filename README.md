# Leetcode solving


# Todos minimum common plan
- [x] 437. Path Sum III
- [x] 74. Search a 2D Matrix
- [x] 33. Search in Rotated Sorted Array
- [x] 108. Convert Sorted Array to Binary Search Tree
- [x] 230. Kth Smallest Element in a BST
- [x] 173. Binary Search Tree Iterator
- [x] 994. Rotting Oranges
- [x] 417. Pacific Atlantic Water Flow
- [x] 210. Course Schedule II
<!-- - [ ] ----- 815. Bus Routes -- 43 / 49 testcases passed -->
- [x] 198. House Robber
- [x] 322. Coin Change
- [x] 416. Partition Equal Subset Sum
- [x] 152. Maximum Product Subarray
- [x] 3. Longest Substring Without Repeating Characters
- [x] 16. 3Sum Closest
<!-- - [ ] 76. Minimum Window Substring -->
- [x] 100. Same Tree
- [x] 101. Symmetric Tree
- [x] 199. Binary Tree Right Side View
- [x] 232. Implement Queue using Stacks
- [x] 155. Min Stack
- [x] 208. Implement Trie (Prefix Tree)
- [ ] 57. Insert Interval
- [ ] 56. Merge Intervals
- [ ] 735. Asteroid Collision
- [ ] 227. Basic Calculator II
- [ ] 547. Number of Provinces
- [ ] 947. Most Stones Removed with Same Row or Column
- [ ] 39. Combination Sum
- [ ] 46. Permutations