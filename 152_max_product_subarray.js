/**
 * @param {number[]} nums
 * @return {number}
 */
var maxProduct = function(nums) {
  let rs = nums[0];

  let localMax = 1;
  let localMin = 1;

  for (let i=0; i<nums.length; i++) {
      console.log({num: nums[i], localMax, localMin, rs})

      const tmp = localMax;

      localMax = Math.max(localMax * nums[i], localMin*nums[i], nums[i]);
      localMin = Math.min(tmp * nums[i], localMin*nums[i], nums[i]);

      rs = Math.max(rs, localMax);
  }

  return rs;
};










/**
 * @param {number[]} nums
 * @return {number}
 */
var maxProduct = function(nums) {
  let maxMpl = Number. MIN_SAFE_INTEGER;

  for (let i = 0; i < nums.length; i++){
      let tmpMpl = nums[i];
      let cumMpl = nums[i];

      for (let k = i+1; k < nums.length; k++){
          if (cumMpl * nums[k] > tmpMpl) {
              tmpMpl = cumMpl * nums[k];
          }

          cumMpl = cumMpl * nums[k];
      }

      if (maxMpl < tmpMpl) maxMpl = tmpMpl;
  }

  return maxMpl;

  // console.log(st);

  // return Math.max(...st.keys())
};