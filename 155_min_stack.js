
var MinStack = function() {
  // TODO: move to one-way linked list
  this.minList = [];
  this.list = [];
};

/** 
* @param {number} val
* @return {void}
*/
MinStack.prototype.push = function(val) {
  this.list.push(val);
  if (this.minList.length === 0) {
      this.minList.push(val);
  } else {
      const lastMin = this.minList[this.minList.length - 1];
      if (lastMin > val) {
          this.minList.push(val);
      }
      else {
          this.minList.push(lastMin);
      }
  }
};

/**
* @return {void}
*/
MinStack.prototype.pop = function() {
  this.minList.pop();
  return this.list.pop();
};

/**
* @return {number}
*/
MinStack.prototype.top = function() {
  return this.list[this.list.length - 1];
};

/**
* @return {number}
*/
MinStack.prototype.getMin = function() {
  return this.minList[this.minList.length - 1];
};

/** 
* Your MinStack object will be instantiated and called as such:
* var obj = new MinStack()
* obj.push(val)
* obj.pop()
* var param_3 = obj.top()
* var param_4 = obj.getMin()
*/