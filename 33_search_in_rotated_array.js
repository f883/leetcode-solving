/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number}
 */
var search = function(nums, target) {
  let lp = 0;
  let rp = nums.length - 1;
  let cp;

  // console.log(nums)

  while (lp <= rp){
      cp = Math.round((lp + rp) / 2);

      // console.log({
      //     lp,
      //     cp,
      //     rp,
      //     lVal: nums[lp],
      //     cVal: nums[cp],
      //     rVal: nums[rp],
      // })

      if (nums[cp] === target) return cp;
      // if (nums[rp] === target) return rp;

      // left sorted
      if (nums[lp] < nums[cp]){
          if (target < nums[lp] || target > nums[cp]) {
              lp = cp + 1;
          }
          else {
              rp = cp - 1;
          }
      } else {
          if (target > nums[rp] || target < nums[cp]) {
              rp = cp - 1;
          }
          else {
              lp = cp + 1;
          }
      }
  }

  if (lp === rp && nums[lp] === target) return lp;

  return -1;
};