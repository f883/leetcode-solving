var MyQueue = function() {
    this.firstStack = [];
    this.secondStack = [];
};

/** 
 * @param {number} x
 * @return {void}
 */
MyQueue.prototype.push = function(x) {
    while (this.secondStack.length !== 0){
        this.firstStack.push(this.secondStack.pop());
    }

    this.firstStack.push(x);
    this.secondStack = [];

    while (this.firstStack.length !== 0){
        this.secondStack.push(this.firstStack.pop());
    }
};

/**
 * @return {number}
 */
MyQueue.prototype.pop = function() {
    return this.secondStack.pop();
};

/**
 * @return {number}
 */
MyQueue.prototype.peek = function() {
    return this.secondStack[this.secondStack.length - 1];
};

/**
 * @return {boolean}
 */
MyQueue.prototype.empty = function() {
    return !this.secondStack.length;
};

/** 
 * Your MyQueue object will be instantiated and called as such:
 * var obj = new MyQueue()
 * obj.push(x)
 * var param_2 = obj.pop()
 * var param_3 = obj.peek()
 * var param_4 = obj.empty()
 */