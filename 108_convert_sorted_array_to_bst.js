/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {number[]} nums
 * @return {TreeNode}
 */
var sortedArrayToBST = function(nums) {
  function build(left, right) {
      let center = Math.floor((left + right) / 2);

      if (left === right) return new TreeNode(nums[center]);
      if (left >= right) return null;

      const res = new TreeNode(nums[center]);

      res.left = build(left, center - 1);
      res.right = build(center + 1, right);

      return res;
  }

  return build(0, nums.length - 1);
};