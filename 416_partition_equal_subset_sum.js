/**
 * @param {number[]} nums
 * @return {boolean}
 */
var canPartition = function(nums) {
  // если сумма нечетная, вернуть фалс
  // дальше, ищем сумму, равную половине суммы

  const sum = nums.reduce((a, b) => a + b, 0);

  console.log(sum);

  if (sum % 2 === 1) return false;

  const target = sum / 2;

  const set = new Set();

  set.add(0);

  nums.forEach(num => {
    const tmp = [...set];

    tmp.forEach(setItem => {
      set.add(setItem + num);
    })
  })

  if (set.has(target)) return true 
  else return false;

  // function run(index = 0, sum = 0){
  //   // if (index === nums.length) return false;

  //   // console.log({index, sum});
  //   if (sum > target) return false;
  //   if (sum === target) return true;

  //   let isFound = false;

  //   for (let i = index + 1; i <= nums.length; i++){
  //     isFound = run(i, nums[i] + sum) || isFound;
  //   }

  //   return isFound;
  // }

  // return run();
};


// const rs = canPartition([1,5,11,5]);
// const rs = canPartition([1,2,3,5]);
const rs = canPartition([100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,100,99,97]);




console.log(rs);
