/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @return {number[]}
 */
var rightSideView = function(root) {
    
    
  const res = [];
  let depthRight = 0;

  function dfsRight(node, depth = 0){
      if (!node) return;

      // console.log({v: node.val})

      res.push(node.val);
      depthRight++;

      dfsRight(node.right, depth + 1);
  }

  function dfsOther(node, depth = 0){
      if (!node) return;


      if (depth >= depthRight){
          res.push(node.val);
          depthRight++;
      }

      dfsOther(node.right, depth + 1);
      dfsOther(node.left, depth + 1);
  }

  dfsRight(root);

  dfsOther(root);

  return res;
};