/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 */
var BSTIterator = function(root) {
  this.arr = [];
  this.curIndex = 0;

  const run = (node) => {
      if (!node) return;

      run(node.left);
      
      this.arr.push(node.val);
      
      run(node.right);
  }

  run(root);

  return null;
};

/**
* @return {number}
*/
BSTIterator.prototype.next = function() {
  return this.arr[this.curIndex++];
};

/**
* @return {boolean}
*/
BSTIterator.prototype.hasNext = function() {
  return Number.isInteger(this.arr[this.curIndex]);
};

/** 
* Your BSTIterator object will be instantiated and called as such:
* var obj = new BSTIterator(root)
* var param_1 = obj.next()
* var param_2 = obj.hasNext()
*/