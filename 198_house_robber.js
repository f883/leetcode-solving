/**
 * @param {number[]} nums
 * @return {number}
 */
var rob = function(nums) {
  const map = {};

  function run(index = 0, sum = 0){
      if (Number.isInteger(map[index])) return map[index];

      let max = 0;

      for (let i = index + 2; i < nums.length; i++){
          const t = run(i, sum);
          if (t > max) max = t;
      }

      const rs = sum + max + nums[index];

      map[index] = rs;

      return rs;
  }

  const first = run();
  const second = run(1);

  return first < second ? second : first;
};