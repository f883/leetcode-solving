/**
 * @param {string} s
 * @return {number}
 */
var lengthOfLongestSubstring = function(s) {

  const st = new Set();

  let maxLength = 0;
  let startIndex = 0;

  for(let i = 0; i < s.length; i++){

    console.log({i, v: s[i], maxLength, st});

    if (st.has(s[i])){
      maxLength = Math.max(maxLength, i - startIndex);

      while(startIndex !== i && st.has(s[i])){
        st.delete(s[startIndex]);

        startIndex++;
      }

    }
    
    st.add(s[i]);

    console.log({i, v: s[i], maxLength, st});

    console.log('==================================');
  }

  maxLength = Math.max(maxLength, st.size);

  return maxLength;
};

const rs = lengthOfLongestSubstring('pwwkew');
// const rs = lengthOfLongestSubstring('bbbbb');
// const rs = lengthOfLongestSubstring('dvdf');


console.log(rs);