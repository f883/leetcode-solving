

var coinChange = function (coins, amount) {

  const mem = {};

  function run(sum){
    // рекурсивно проходим все возможные варианты до конца
    // если конец есть, возвращаем 0, это первый уровень
    // каждый следующий уровень накидывается в момент вызова функции, 
    // строя таким образом цепочку вглубь, начиная с самого конца
    
    if (sum < 0) return Number.MAX_SAFE_INTEGER;
    if (sum === 0) return 0;
    if (mem[sum]) return mem[sum]; 

    const leastCoins = [];
    
    coins.forEach(coin => {
      leastCoins.push(run(sum - coin) + 1);
    })
    
    mem[sum] = Math.min(...leastCoins);

    return mem[sum];
  }

  const res = run(amount);

  // console.log({mem})

  return res < Number.MAX_SAFE_INTEGER ? res : -1;
}


const rs = coinChange([2], 11);
// const rs = coinChange2([83, 186, 408, 419], 6249);

// const rs = coinChange([2], 3);

console.log(rs);
