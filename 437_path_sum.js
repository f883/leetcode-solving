/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} targetSum
 * @return {number}
 */
var pathSum = function(root, targetSum) {
  let count = 0;

  const runToLeaf = (node, sum = 0) => {
      if (!node) return;

      const newSum = sum + node.val;

      if (newSum === targetSum) count++;

      runToLeaf(node.left, newSum);
      runToLeaf(node.right, newSum);
  }

  const runFromVertex = (node) => {
      if (!node) return;

      runToLeaf(node);

      runFromVertex(node.left);
      runFromVertex(node.right);
  }

  runFromVertex(root);

  return count;
};