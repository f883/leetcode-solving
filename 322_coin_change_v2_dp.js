

var coinChange = function (coins, amount) {
  
  // задача -- просчитать все наименьшие комбинации монет, начиная с нуля 
  // 

  const arr = new Array(amount + 1).fill(Number.MAX_SAFE_INTEGER);
  arr[0] = 0;

  // для каждого элемента массива, начиная с нуля, посчитать кратчайший способ получить сумму

  for(let curNumber = 1; curNumber <= amount; curNumber++){
    coins.forEach(c => {
      if (curNumber - c >= 0){ // Исключаем возможность выхода в отрицательные индексы 

        // Если значение уже задано на проходе раньше, оставляем наименьшее значение
        // Если не найдено, ищем 

        arr[curNumber] = Math.min(arr[curNumber], 1 + arr[curNumber - c])
      }
    });
  }

  if (arr[amount] === Number.MAX_SAFE_INTEGER) return -1;
  else return arr[amount];
}


// const rs = coinChange([2], 4);
const rs = coinChange([83, 186, 408, 419], 6249);

// const rs = coinChange([2], 3);

console.log(rs);
