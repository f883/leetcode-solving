/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} root
 * @param {number} k
 * @return {number}
 */
var kthSmallest = function(root, k) {
  let arr = [];

  function run(node) {
      if (!node) return;

      if (arr.length > k) {
          return;
      };

      run(node.left);
      
      if (arr.length > k) {
          return;
      };

      arr.push(node.val);

      run(node.right);

      if (arr.length > k) {
          return;
      };

  }

  run(root);

  return arr[k - 1];
};