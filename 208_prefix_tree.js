
var Trie = function() {
  this.tree = {};
};

/** 
* @param {string} word
* @return {void}
*/
Trie.prototype.insert = function(word) {
  let curLeaf = this.tree;

  for (let i = 0; i < word.length; i++){
      if (!curLeaf[word[i]]){
          curLeaf[word[i]] = {};
      }

      curLeaf = curLeaf[word[i]];
  }

  curLeaf.isEnd = true;

  // console.log(this.tree);
};

/** 
* @param {string} word
* @return {boolean}
*/
Trie.prototype.search = function(word) {
  function runSearch(tree, index){
      // console.log('search', {tree, index, l: word.length});

      if (tree.isEnd && index === word.length) return true;


      if (tree[word[index]]) return runSearch(tree[word[index]], index + 1);
      else return false;
  }

  return runSearch(this.tree, 0);
};

/** 
* @param {string} prefix
* @return {boolean}
*/
Trie.prototype.startsWith = function(prefix) {
  function runPrefix(tree, index){
      // console.log('search', {tree, index, l: prefix.length});

      if (index === prefix.length) return true;
      if (tree[prefix[index]]) return runPrefix(tree[prefix[index]], index + 1);
      else return false;
  }

  return runPrefix(this.tree, 0);
};

/** 
* Your Trie object will be instantiated and called as such:
* var obj = new Trie()
* obj.insert(word)
* var param_2 = obj.search(word)
* var param_3 = obj.startsWith(prefix)
*/