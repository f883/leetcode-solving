/**
 * Definition for a binary tree node.
 * function TreeNode(val, left, right) {
 *     this.val = (val===undefined ? 0 : val)
 *     this.left = (left===undefined ? null : left)
 *     this.right = (right===undefined ? null : right)
 * }
 */
/**
 * @param {TreeNode} p
 * @param {TreeNode} q
 * @return {boolean}
 */
var isSameTree = function(p, q) {
    

  function dfs(pNode, qNode) {
      if (!pNode && !qNode) return true;
      else if (!pNode || !qNode) return false;
      else if (pNode.val !== qNode.val) return false;

      const pRes = dfs(pNode.left, qNode.left);
      const qRes = dfs(pNode.right, qNode.right);

      return pRes & qRes;
  }

  return dfs(p, q);
};