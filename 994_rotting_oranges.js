/**
 * @param {number[][]} grid
 * @return {number}
 */
var orangesRotting = function(grid) {
  let time = 0;
  let didRottened = true;
  let aliveCount = 0;
  let gridCloned;

  for (let i = 0; i < grid.length; i++){
      for (let j = 0; j < grid[i].length; j++){
          if (grid[i][j] === 1) {
              aliveCount++;
          }
      }
  }

  if (aliveCount === 0) return 0;

  while (didRottened){
      time++;

      didRottened = false;
      
      // gridCloned = [];

      // for (let i = 0; i < grid.length; i++){
      //     gridCloned.push(grid[i].map(a => {return a}));
      // }
      
      gridCloned = JSON.parse(JSON.stringify(grid)); // TODO: optimize cloning

      for (let i = 0; i < grid.length; i++){
          for (let j = 0; j < grid[i].length; j++){
              if (grid[i][j] === 2) {
                  if (gridCloned[i + 1] && gridCloned[i + 1][j] === 1) {
                      gridCloned[i + 1][j]++;
                      didRottened = true;
                      aliveCount--;
                  }

                  if (gridCloned[i][j + 1] === 1) {
                      gridCloned[i][j + 1]++;
                      didRottened = true;
                      aliveCount--;
                  }

                  if (gridCloned[i - 1] && gridCloned[i - 1][j] === 1) {
                      gridCloned[i - 1][j]++;
                      didRottened = true;
                      aliveCount--;
                  }

                  if (gridCloned[i][j - 1] === 1) {
                      gridCloned[i][j - 1]++;
                      didRottened = true;
                      aliveCount--;
                  }
              }
          }
      }

      if (aliveCount === 0 || !didRottened) break;

      grid = gridCloned;
  }

  return aliveCount === 0 ? time : -1;
};