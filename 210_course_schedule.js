/**
 * @param {number} numCourses
 * @param {number[][]} prerequisites
 * @return {number[]}
 */
var findOrder = function(numCourses, prerequisites) {
  // TODO: speed up this

  let isCycleFound = false;
  const seq = [];
  const mapReq = {};
  const mapVisited = {};

  for (let i = 0; i < numCourses; i++){
      mapReq[i] = [];
  }

  for (let i = 0; i < prerequisites.length; i++){
      const ind = prerequisites[i][0];
      mapReq[ind].push(prerequisites[i][1]);
  }

  function dfs(nodeIndex, visitedList = []){
      if (mapVisited[nodeIndex]) return;

      if (visitedList.includes(nodeIndex)) {
          isCycleFound = true;
          return;
      };

      const newVisited = [...visitedList, nodeIndex];

      for (let i = 0; i < mapReq[nodeIndex].length; i++){
          dfs(mapReq[nodeIndex][i], newVisited);
          if (isCycleFound) return;
      }

      mapVisited[nodeIndex] = true;
      seq.push(nodeIndex);
  }

  for (let i = 0; i < numCourses; i++){
      dfs(i);
      if (isCycleFound) return [];
  }

  return seq;
};